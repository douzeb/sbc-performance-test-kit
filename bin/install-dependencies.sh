#!/bin/bash

apt -y install make automake libtool pkg-config libaio-dev libssl-dev smartmontools hdparm nvme-cli
#apt -y install libmysqlclient-dev 
apt -y install libmariadb-dev libmariadb-dev-compat
apt -y install libpq-dev
apt -y install lm-sensors
git clone https://github.com/akopytov/sysbench
cd sysbench/
./autogen.sh
./configure
make -j
make install
cd ..
