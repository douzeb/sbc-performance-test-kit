#!/bin/bash

TEST_DISK_SPACE="64G"
TEST_FILE_NUM=512

if [[ $# -lt 1 ]] ; then
  echo "\\o/"
  echo "Not enough arguments supplied" >&2
  echo "You need to pass a name to store the test results" >&2
  echo " |" >&2
  echo "syntax: " >&2
  echo "  $(basename $0) <test-name> " >&2
  echo "example: " >&2
  echo "  $(basename $0) odroid-h2+-emmc " >&2
  echo "/!\\" >&2
  exit 1
fi

SCRIPTDIR=$(dirname $0)/..
REPODIR=$(readlink -f $SCRIPTDIR/..)
mkdir -p $REPODIR/sbc-performance-test-kit/benchmark
LOGFILE="$REPODIR/sbc-performance-test-kit/benchmark/quick_test_$1_$(hostname)-$(date '+%Y-%m-%d_%H-%M-%S').log"

echo ' '
echo "#### Starting benchmark ####" | tee $LOGFILE
echo TS=$(date  --rfc-3339=ns) | tee -a $LOGFILE
echo TEST_DISK_SPACE=$TEST_DISK_SPACE | tee -a $LOGFILE
echo TEST_FILE_NUM=$TEST_FILE_NUM | tee -a $LOGFILE
echo ' ' | tee -a $LOGFILE
sensors 2>&1 | tee -a $LOGFILE

echo ' ' | tee -a $LOGFILE
echo "### System information ###" | tee -a $LOGFILE

echo "# uname -a #" | tee -a $LOGFILE
uname -a | tee -a $LOGFILE
echo "# /proc/version #" | tee -a $LOGFILE
cat /proc/version | tee -a $LOGFILE
echo "# /proc/meminfo #" | tee -a $LOGFILE
grep MemTotal /proc/meminfo | tee -a $LOGFILE

echo "# /proc/cpuinfo #" | tee -a $LOGFILE
cat /proc/cpuinfo | tee -a $LOGFILE
echo "# cpufreq-info" | tee -a $LOGFILE
cpufreq-info 2>&1 | tee -a $LOGFILE

echo "# Block devices #" | tee -a $LOGFILE
lsblk | grep -v loop | tee -a $LOGFILE
echo "# df --block-size=G $(pwd) #" | tee -a $LOGFILE
df --block-size=G $(pwd)  | tee -a $LOGFILE
BLOCK_DEVICES=$(lsblk | grep -v loop | grep disk | cut -f1 -d ' ')
for d in $BLOCK_DEVICES
do
  echo "# block device /dev/$d => hdparm -Iv #" | tee -a $LOGFILE
  sudo hdparm -Iv /dev/$d 2>&1 | tee -a $LOGFILE
done
for d in $BLOCK_DEVICES
do
  echo "# block device /dev/$d => smartctl -x #" | tee -a $LOGFILE
  sudo smartctl -x /dev/$d
done

echo ' ' | tee -a $LOGFILE
echo "#### CPU ####" | tee -a $LOGFILE
sensors 2>&1  | tee -a $LOGFILE
/usr/local/bin/sysbench cpu run --threads=8 2>&1  | tee -a $LOGFILE
sensors 2>&1  | tee -a $LOGFILE

echo "#### RAM ####" | tee -a $LOGFILE
sensors 2>&1 | tee -a $LOGFILE
/usr/local/bin/sysbench memory run --threads=8 2>&1 | tee -a $LOGFILE
sensors 2>&1 | tee -a $LOGFILE

echo calling $(dirname $0)/disk-test.sh $LOGFILE $TEST_DISK_SPACE $TEST_FILE_NUM | tee -a $LOGFILE
$(dirname $0)/disk-test.sh $LOGFILE $TEST_DISK_SPACE $TEST_FILE_NUM

echo TS=$(date  --rfc-3339=ns) | tee -a $LOGFILE
echo "#### Tests complete ####" | tee -a $LOGFILE
